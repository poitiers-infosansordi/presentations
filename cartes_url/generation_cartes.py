import itertools
from reportlab.lib.pagesizes import A4
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch, cm
from reportlab.lib.utils import ImageReader


def carte(cvs, x, y):
    canvas.setFont('Helvetica', 9)
    cvs.drawString(x, y, 'Laurent.Signac@univ-poitiers.fr')
    y += 12
    cvs.drawString(x, y, 'Samuel.Peltier@univ-poitiers.fr')
    y += 12
    cvs.drawString(x, y, 'Sylvie.Alayrangues@univ-poitiers.fr')
    y += 8
    img = ImageReader("qrcode.png")
    cvs.drawImage(img, x+35, y, 100, 100)
    y+= 105
    cvs.drawString(x, y, 'https://framagit.org/poitiers-infosansordi/')

canvas = canvas.Canvas("cartes_visite.pdf", pagesize=A4)
canvas.setLineWidth(.3)
canvas.setFont('Helvetica', 12)

for x in (10, 210, 410):
    for y in (10, 180, 350, 520, 690):# (100, 200, 300, 400, 500, 600, 700, 800):
        carte(canvas, x, y)

canvas.save()
